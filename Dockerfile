FROM python:3

WORKDIR /app

COPY req.txt req.txt
RUN pip install --no-cache-dir -r req.txt

COPY . .
