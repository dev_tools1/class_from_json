PYTHON=python3
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c

.PHONY: build
run:
	@docker-compose up -d --build --force-recreate --remove-orphans

.PHONY: logs
logs:
	@docker-compose  logs -f

.PHONY: stop
stop:
	@docker-compose  down --remove-orphans

.PHONY: unittest
unittest:
	@docker-compose run --rm --no-deps app python3 -m unittest

.PHONY: flake
flake:
	@docker-compose run --rm --no-deps app flake8 ./*

.PHONY: bandit
bandit:
	@docker-compose run --rm --no-deps app bandit ./*

.PHONY: class
class:
	@docker-compose run --rm --no-deps app python3 app.py $(config_file)


