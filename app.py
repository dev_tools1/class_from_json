from factory import StrategyFactory
import generator
import json
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("config_file", help="Config file json")

    args = parser.parse_args()
    data = _file_to_json(args.config_file)

    namespace = data['namespace']

    field_s = StrategyFactory.build('field_strategy', _to_class_name(data['field_strategy']))
    getter = StrategyFactory.build('getter_strategy', _to_class_name(data['getter_strategy']))
    setter = StrategyFactory.build('setter_strategy', _to_class_name(data['setter_strategy']))
    ser = StrategyFactory.build('serializer_strategy', _to_class_name(data['serializer_strategy']))
    file = StrategyFactory.build('file_name_strategy', _to_class_name(data['file_name_strategy']))
    null = StrategyFactory.build('null_strategy', _to_class_name(data['null_strategy']))

    json_tr = generator.JsonTransformer(field_s, getter, setter, ser)
    dto_tr = generator.DtoTransformer(null)
    templ_tr = generator.TemplateTransformer(file)

    dto_source = json_tr.to_dto(_file_to_json(data['json_source']), namespace, 'RootClass')
    dto_null = json_tr.to_dto(_file_to_json(data['nullable_json_source']), namespace, 'RootClass')
    dto = dto_tr.merge_nulls(dto_source, dto_null)

    template = dto_tr.to_template_data(dto)
    templ_tr.save_to_dir(template, data['save_dir'], data['class_template'], '.php')

def _to_class_name(name: str)-> str:
    name_el = name.split('.')[0].split('_')
    class_name = ''
    for el in name_el:
        class_name += el[0].upper() + el[1:]

    return class_name

def _file_to_json(name):
    with open(name) as f:
        f_json = json.load(f)

    return f_json

if __name__ == '__main__':
    main()