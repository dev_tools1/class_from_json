from dataclasses import dataclass, field


@dataclass
class ClassDto:
    namespace: str
    name: str
    dependencies: list = field(default_factory=list)
    setters: list = field(default_factory=list)
    getters: list = field(default_factory=list)
    fields: dict = field(default_factory=dict)

    def add_field(self, field_dto):
        self.fields[field_dto.name] = field_dto
        return self

    def add_setter(self, setter):
        self.setters.append(setter)
        return self

    def add_getter(self, getter):
        self.getters.append(getter)
        return self

    def add_dependency(self, dependency):
        self.dependencies.append(dependency)
        return self


@dataclass
class FieldDto:
    access: str
    name: str
    field_type: str
    init_value: str or None = None
    serialization: str or None = None


@dataclass
class GetterDto:
    field: FieldDto
    access: str
    func_name: str


@dataclass
class ReqDependenciesDto:
    version: str
    name: str
    use_phrase: str


@dataclass
class SetterDto:
    __slots__ = ['field', 'access', 'func_name', 'is_fluent', 'return_type']
    field: FieldDto
    access: str
    func_name: str
    is_fluent: bool
    return_type: str
