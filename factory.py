import strategies


class StrategyFactory:

    @staticmethod
    def build(strategy_type, class_name):
        path = [strategy_type, class_name]
        obj = strategies
        for x in path:
            obj = getattr(obj, x)

        return obj()
