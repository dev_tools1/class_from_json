import dto
import copy
from strategies.null_strategy import NullStrategyInterface


class DtoTransformer:
    def __init__(self, null_strategy: NullStrategyInterface = None):
        self.null_strategy = null_strategy
        self.template_data_schema = {
            'namespace': '',
            'class_name': '',
            'imports': [],
            'fields_list': [],
            'functions': []
        }
        self.templates = []

    def to_template_data(self, class_dtos: list) -> list:
        class_data: dto.ClassDto
        for class_data in class_dtos:
            template_data = copy.deepcopy(self.template_data_schema)
            template_data['class_name'] = class_data.name
            template_data['namespace'] = class_data.namespace
            for (field_name, field), setter, getter in zip(
                    class_data.fields.items(),
                    class_data.setters,
                    class_data.getters
            ):
                template_data['fields_list'].append(self._fulfill_field_data(field))

                functions = {'setter': self._fulfill_setter_data(setter), 'getter': self._fulfill_getter_data(getter)}
                template_data['functions'].append(functions)

            for dependency in class_data.dependencies:
                template_data['imports'].append(dependency.use_phrase)

            self.templates.append(template_data)

        return self.templates

    def merge_nulls(self, source_class_dtos: dict, null_class_dtos: dict) -> list:
        if self.null_strategy is None:
            raise Exception('There is no strategy for nullification')

        class_with_nullables = []
        for name, class_data in source_class_dtos.items():
            nullable_fields = {}

            source_class_copy = copy.deepcopy(class_data)
            if null_class_dtos.get(name) is None:
                class_with_nullables.append(source_class_copy)
                continue

            null_class_copy = copy.deepcopy(null_class_dtos[name])
            for field_name, field in source_class_copy.fields.items():
                if null_class_copy.fields.get(field_name) is None:
                    nullable_fields[field_name] = field
                    continue

                nullable_fields[field_name] = self.null_strategy.make_nullable(field)

            source_class_copy.fields = nullable_fields
            class_with_nullables.append(source_class_copy)

        return class_with_nullables

    def _fulfill_getter_data(self, getter: dto.GetterDto) -> dict:
        return {
            'func_name': getter.func_name,
            'name': getter.field.name,
            'return_type': getter.field.field_type #TODO it shouldn't be required, sometimes there is no return type: e.g. php ~5.4
        }

    def _fulfill_setter_data(self, setter: dto.SetterDto) -> dict:
        return {
            'func_name': setter.func_name,
            'field_type': setter.field.field_type,
            'name': setter.field.name,
            'is_fluent': setter.is_fluent,
            'return_type': setter.return_type
        }

    def _fulfill_field_data(self, field: dto.FieldDto) -> dict:
        field_data = {
            'field_type': field.field_type,
            'name': field.name,
            'access': field.access,
            'serialization': field.serialization,
        }
        if field.init_value is not None:
            field_data['init'] = field.init_value

        return field_data
