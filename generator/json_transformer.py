import strategies
import dto


class JsonTransformer:
    def __init__(
            self,
            field_strategy: strategies.field_strategy.FieldStrategyInterface,
            getter_strategy: strategies.getter_strategy.GetterStrategyInterface,
            setter_strategy: strategies.setter_strategy.SetterStrategyInterface,
            serializer_strategy: strategies.serializer_strategy.SerializerStrategyInterface
    ):
        self.field_strategy = field_strategy
        self.getter_strategy = getter_strategy
        self.setter_strategy = setter_strategy
        self.serializer_strategy = serializer_strategy

    def to_dto(self, source_file: dict, namespace='', root_name='RootClass') -> dict:
        class_dtos = {}
        return self._transform_into_class_dto(root_name, source_file, namespace, class_dtos)

    def _transform_into_class_dto(self, name: str, value: dict, namespace: str, class_dtos: dict) -> dict:
        name = self._to_upper_case_name(name)
        root_class = dto.ClassDto(
            name=name,
            namespace=namespace,
            dependencies=self.serializer_strategy.required_dependencies()
        )

        for key, value in value.items():
            if type(value) is dict:
                class_dtos = self._transform_into_class_dto(key, value, namespace, class_dtos)

            if type(value) is list and len(value) > 0 and type(value[0]) is dict:
                class_dtos = self._transform_into_class_dto(key, value[0], namespace, class_dtos)

            field = self.field_strategy.build_field(key, value)
            field = self.serializer_strategy.add_serialization(field, root_class)

            root_class = root_class \
                .add_field(field) \
                .add_getter(self.getter_strategy.build_getter(field)) \
                .add_setter(self.setter_strategy.build_setter(field, root_class))

        class_dtos[name] = root_class
        return class_dtos

    def _to_upper_case_name(self, name: str) -> str:
        return name[0].upper() + name[1:]
