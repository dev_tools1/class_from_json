from strategies.file_name_strategy import FileStrategyInterface
from jinja2 import Template


class TemplateTransformer:
    def __init__(self, file_name_strategy: FileStrategyInterface):
        self.file_name_strategy = file_name_strategy

    def save_to_dir(self, templates: list, save_dir: str, template_dir_path: str, ext: str) -> None:
        template_source = open(template_dir_path).read()
        for template in templates:
            file_name = self.file_name_strategy.build_file_name(template['class_name'], ext)
            jinja_template = Template(template_source)
            with open(save_dir + file_name, "w") as fh:
                fh.write(jinja_template.render(template))

        print('All DONE')
