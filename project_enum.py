from enum import Enum


class ProgrammingLanguageEnum(Enum):
    PHP_PROGRAMMING_LANG = 1
    PYTHON_PROGRAMMING_LANG = 2


class DataTypeEnum(Enum):
    STRING_DATA_TYPE = 1
    DICTIONARY_DATA_TYPE = 2
    LIST_DATA_TYPE = 3

    DATA_TYPE_PER_PROG_LANG = {
        ProgrammingLanguageEnum.PHP_PROGRAMMING_LANG: {
            r"<class 'str'>": 'string',
            r"<class 'list'>": 'array',
            r"<class 'int'>": 'intr',
            r"<class 'float'>": 'float',
            r"<class 'bool'>": 'boolean',
            r"<class 'NoneType'>": 'null',
        }}

    @staticmethod
    def get_type_for_provided_lang(prog_lang: ProgrammingLanguageEnum, type: str) -> str:
        return DataTypeEnum.DATA_TYPE_PER_PROG_LANG.value[prog_lang][fr"{type}"]
