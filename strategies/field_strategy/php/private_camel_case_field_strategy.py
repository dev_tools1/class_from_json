import dto
import project_enum


class PrivateCamelCaseFieldStrategy:
    @staticmethod
    def supported_prog_lang() -> project_enum.ProgrammingLanguageEnum:
        return project_enum.ProgrammingLanguageEnum.PHP_PROGRAMMING_LANG

    @staticmethod
    def supported_min_ver() -> str:
        return '7.4'

    def build_field(self, key: str, value) -> dto.FieldDto:
        name = key

        return dto.FieldDto(
            'private',
            name,
            self._retrieve_field_type(name, value)
        )

    def _retrieve_field_type(self, name: str, value) -> str:
        if type(value) is dict:  # it means it is class so we want it in CamelCase
            return self._build_class_name(name)

        field_type = project_enum.DataTypeEnum.get_type_for_provided_lang(self.supported_prog_lang(), type(value))
        if field_type != 'array':
            return field_type

        if len(value) != 0 and type(value[0]) is dict:  # array of classes
            return self._build_class_name(name) + '[]'

        return field_type  # always `array`

    def _build_class_name(self, name: str) -> str:
        return ''.join(map(lambda x: x[0].upper() + x[1:], name.split('_')))