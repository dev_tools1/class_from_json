import abc


class FileStrategyInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'build_file') and
                callable(subclass.build_file)
                )
