from strategies.file_name_strategy.file_name_strategy_interface import FileStrategyInterface


class SameAsClassNameStrategy(FileStrategyInterface):
    def build_file_name(self, file_name: str, ext: str) -> str:
        return file_name + ext