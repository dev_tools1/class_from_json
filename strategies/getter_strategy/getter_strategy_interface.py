import abc


class GetterStrategyInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'supported_prog_lang') and
                callable(subclass.supported_prog_lang) and
                hasattr(subclass, 'supported_min_ver') and
                callable(subclass.supported_min_ver) and
                hasattr(subclass, 'build_getter') and
                callable(subclass.build_getter)
                )
