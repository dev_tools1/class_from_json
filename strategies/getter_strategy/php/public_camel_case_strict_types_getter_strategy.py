from strategies.getter_strategy.getter_strategy_interface import GetterStrategyInterface
import dto
from project_enum import ProgrammingLanguageEnum


class PublicCamelCaseStrictTypesGetterStrategy(GetterStrategyInterface):
    @staticmethod
    def supported_prog_lang() -> ProgrammingLanguageEnum:
        return ProgrammingLanguageEnum.PHP_PROGRAMMING_LANG

    @staticmethod
    def supported_min_ver() -> str:
        return '7.4'

    def build_getter(self, field: dto.FieldDto) -> dto.GetterDto:
        name = field.name
        return dto.GetterDto(
            field,
            'public',
            'get%s' % name[0].upper() + name[1:]
        )
