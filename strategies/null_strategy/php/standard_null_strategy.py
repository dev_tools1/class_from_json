import dto
from project_enum import ProgrammingLanguageEnum
from strategies.null_strategy.null_strategy_interface import NullStrategyInterface


class StandardNullStrategy(NullStrategyInterface):
    @staticmethod
    def supported_prog_lang() -> ProgrammingLanguageEnum:
        return ProgrammingLanguageEnum.PHP_PROGRAMMING_LANG

    @staticmethod
    def supported_min_ver() -> str:
        return '7.4'

    def make_nullable(self, field: dto.FieldDto) -> dto.FieldDto:
        field.field_type = '?' + field.field_type
        field.init_value = 'null' if not field.init_value else field.init_value
        return field
