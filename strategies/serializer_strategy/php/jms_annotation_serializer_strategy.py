from strategies.serializer_strategy.serializer_strategy_interface import SerializerStrategyInterface
import dto
from project_enum import ProgrammingLanguageEnum


class JmsAnnotationSerializerStrategy(SerializerStrategyInterface):
    @staticmethod
    def supported_prog_lang() -> ProgrammingLanguageEnum:
        return ProgrammingLanguageEnum.PHP_PROGRAMMING_LANG

    @staticmethod
    def supported_min_ver() -> str:
        return '7.4'

    @staticmethod
    def required_dependencies() -> list:
        return [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")]

    def add_serialization(self, field: dto.FieldDto, class_dto: dto.ClassDto) -> dto.FieldDto:
        if not field.field_type[0].isupper():
            field.serialization = '@Type("' + field.field_type + '")'
            return field

        full_field_type = class_dto.namespace.replace('\\', '/') + '/' + field.field_type
        field.serialization = '@Type("' + full_field_type + '")' \
            if field.field_type.find('[]') == -1 \
            else '@Type("array<' + full_field_type[:-2] + '>")'  # [:-2] because we want to get rid of `[]` from type

        return field
