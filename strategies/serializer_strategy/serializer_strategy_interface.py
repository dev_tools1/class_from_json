import abc


class SerializerStrategyInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'supported_prog_lang') and
                callable(subclass.supported_prog_lang) and
                hasattr(subclass, 'supported_min_ver') and
                callable(subclass.supported_min_ver) and
                hasattr(subclass, 'required_dependencies') and
                callable(subclass.required_dependencies) and
                hasattr(subclass, 'add_serialization') and
                callable(subclass.add_serialization)
                )
