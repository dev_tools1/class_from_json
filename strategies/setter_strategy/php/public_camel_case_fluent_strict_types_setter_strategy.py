from strategies.setter_strategy.setter_strategy_interface import SetterStrategyInterface
from project_enum import ProgrammingLanguageEnum
import dto


class PublicCamelCaseFluentStrictTypesSetterStrategy(SetterStrategyInterface):
    @staticmethod
    def supported_prog_lang() -> ProgrammingLanguageEnum:
        return ProgrammingLanguageEnum.PHP_PROGRAMMING_LANG

    @staticmethod
    def supported_min_ver() -> str:
        return '7.4'

    def build_setter(self, field: dto.FieldDto, class_dto: dto.ClassDto) -> dto.SetterDto:
        name = field.name
        return dto.SetterDto(
            field,
            'public',
            'set%s' % name[0].upper() + name[1:],
            True,
            class_dto.name
        )
