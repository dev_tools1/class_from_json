import unittest
import dto
import generator
from dto import ClassDto
from strategies.null_strategy import StandardNullStrategy


class TestDtoTransformer(unittest.TestCase):

    def test_to_template(self):
        field_one = dto.FieldDto('private', 'test_name_one', 'string', 'null', 'test_ser')
        field_two = dto.FieldDto('private', 'test_name_two', 'array', '[]', 'test_ser')
        field_three = dto.FieldDto('private', 'test_name_three', 'BillingAddress', None, 'test_ser')

        setter_one = dto.SetterDto(field_one, 'public', 'setTest', True, 'self')
        setter_two = dto.SetterDto(field_two, 'public', 'setTest', False, '[]')
        setter_three = dto.SetterDto(field_three, 'public', 'setTest', True, '')

        getter_one = dto.GetterDto(field_one, 'public', 'getTest')
        getter_two = dto.GetterDto(field_two, 'public', 'getTest')
        getter_three = dto.GetterDto(field_three, 'public', 'getTest')

        dependency = dto.ReqDependenciesDto('2', 'test', 'test')
        class_dto = dto.ClassDto(
            name='RootClass',
            namespace='Test/Name/Space',
            dependencies=[dependency],
            setters=[setter_one, setter_two, setter_three],
            getters=[getter_one, getter_two, getter_three],
            fields={'test_name_one': field_one, 'test_name_two': field_two, 'test_name_three': field_three}
        )

        transformer = generator.DtoTransformer()
        template_data = transformer.to_template_data([class_dto])

        self.assertEqual(1, len(template_data))
        schema = template_data[0]
        self.assertEqual('Test/Name/Space', schema['namespace'])
        self.assertEqual('RootClass', schema['class_name'])
        self.assertEqual(['test'], schema['imports'])

        self.assertEqual(3, len(schema['fields_list']))
        field_one = schema['fields_list'][0]
        self.assertEqual('string', field_one['field_type'])
        self.assertEqual('test_name_one', field_one['name'])
        self.assertEqual('private', field_one['access'])
        self.assertEqual('test_ser', field_one['serialization'])
        self.assertEqual('null', field_one['init'])
        field_two = schema['fields_list'][1]
        self.assertEqual('array', field_two['field_type'])
        self.assertEqual('test_name_two', field_two['name'])
        self.assertEqual('private', field_two['access'])
        self.assertEqual('test_ser', field_two['serialization'])
        self.assertEqual('[]', field_two['init'])
        field_three = schema['fields_list'][2]
        self.assertEqual('BillingAddress', field_three['field_type'])
        self.assertEqual('test_name_three', field_three['name'])
        self.assertEqual('private', field_three['access'])
        self.assertEqual('test_ser', field_three['serialization'])
        self.assertEqual(False, 'init' in field_three)

        func = schema['functions']
        self.assertEqual(3, len(func))
        setter_one = func[0]['setter']
        self.assertEqual('setTest', setter_one['func_name'])
        self.assertEqual('string', setter_one['field_type'])
        self.assertEqual('test_name_one', setter_one['name'])
        self.assertEqual(True, setter_one['is_fluent'])
        self.assertEqual('self', setter_one['return_type'])
        getter_one = func[0]['getter']
        self.assertEqual('getTest', getter_one['func_name'])
        self.assertEqual('test_name_one', getter_one['name'])
        self.assertEqual('string', getter_one['return_type'])
        setter_two = func[1]['setter']
        self.assertEqual('setTest', setter_two['func_name'])
        self.assertEqual('array', setter_two['field_type'])
        self.assertEqual('test_name_two', setter_two['name'])
        self.assertEqual(False, setter_two['is_fluent'])
        self.assertEqual('[]', setter_two['return_type'])
        getter_two = func[1]['getter']
        self.assertEqual('getTest', getter_two['func_name'])
        self.assertEqual('test_name_two', getter_two['name'])
        self.assertEqual('array', getter_two['return_type'])
        setter_three = func[2]['setter']
        self.assertEqual('setTest', setter_three['func_name'])
        self.assertEqual('BillingAddress', setter_three['field_type'])
        self.assertEqual('test_name_three', setter_three['name'])
        self.assertEqual(True, setter_three['is_fluent'])
        self.assertEqual('', setter_three['return_type'])
        getter_three = func[2]['getter']
        self.assertEqual('getTest', getter_three['func_name'])
        self.assertEqual('test_name_three', getter_three['name'])
        self.assertEqual('BillingAddress', getter_three['return_type'])

        self.assertEqual(1, len(schema['imports']))
        self.assertEqual('test', schema['imports'][0])

    def test_merge_nulls(self):
        field_one = dto.FieldDto('private', 'test_name_one', 'string', 'null', 'test_ser')
        field_two = dto.FieldDto('private', 'test_name_two', 'array', '[]', 'test_ser')
        field_three = dto.FieldDto('private', 'test_name_three', 'BillingAddress', None, 'test_ser')

        setter_one = dto.SetterDto(field_one, 'public', 'setTest', True, 'self')
        setter_two = dto.SetterDto(field_two, 'public', 'setTest', False, '[]')
        setter_three = dto.SetterDto(field_three, 'public', 'setTest', True, '')

        getter_one = dto.GetterDto(field_one, 'public', 'getTest')
        getter_two = dto.GetterDto(field_two, 'public', 'getTest')
        getter_three = dto.GetterDto(field_three, 'public', 'getTest')

        dependency = dto.ReqDependenciesDto('2', 'test', 'test')
        class_dto: ClassDto = dto.ClassDto(
            name='RootClass',
            namespace='Test/Name/Space',
            dependencies=[dependency],
            setters=[setter_one, setter_two, setter_three],
            getters=[getter_one, getter_two, getter_three],
            fields={'test_name_one': field_one, 'test_name_two': field_two, 'test_name_three': field_three}
        )

        nullable_class_dto = dto.ClassDto(
            name='RootClass',
            namespace='Test/Name/Space',
            dependencies=[dependency],
            setters=[setter_two],
            getters=[getter_two],
            fields={'test_name_two': field_two}
        )

        transformer = generator.DtoTransformer(StandardNullStrategy())
        nullable_classes = transformer.merge_nulls({'RootClass': class_dto}, {'RootClass': nullable_class_dto})

        self.assertEqual(1, len(nullable_classes))
        class_dto = nullable_classes[0]
        self.assertEqual('Test/Name/Space', class_dto.namespace)
        self.assertEqual('RootClass', class_dto.name)

        self.assertEqual(3, len(class_dto.fields))
        field_one = class_dto.fields['test_name_one']
        self.assertEqual('string', field_one.field_type)
        self.assertEqual('test_name_one', field_one.name)
        self.assertEqual('private', field_one.access)
        self.assertEqual('test_ser', field_one.serialization)
        self.assertEqual('null', field_one.init_value)
        field_two = class_dto.fields['test_name_two']
        self.assertEqual('?array', field_two.field_type)
        self.assertEqual('test_name_two', field_two.name)
        self.assertEqual('private', field_two.access)
        self.assertEqual('test_ser', field_two.serialization)
        self.assertEqual('[]', field_two.init_value)
        field_three = class_dto.fields['test_name_three']
        self.assertEqual('BillingAddress', field_three.field_type)
        self.assertEqual('test_name_three', field_three.name)
        self.assertEqual('private', field_three.access)
        self.assertEqual('test_ser', field_three.serialization)
        self.assertEqual(None, field_three.init_value)

    def test_merge_nulls_wo_strategy(self):
        transformer = generator.DtoTransformer()

        with self.assertRaises(Exception) as context:
            transformer.merge_nulls({}, {})
        self.assertEqual('There is no strategy for nullification', str(context.exception))


if __name__ == '__main__':
    unittest.main()
