import unittest
import json
from unittest.mock import MagicMock, call
from strategies.setter_strategy import PublicCamelCaseFluentStrictTypesSetterStrategy
from strategies.getter_strategy import PublicCamelCaseStrictTypesGetterStrategy
from strategies.serializer_strategy import JmsAnnotationSerializerStrategy
from strategies.field_strategy import PrivateCamelCaseFieldStrategy
from generator import JsonTransformer
import dto
from pathlib import Path


class TestJsonTransformer(unittest.TestCase):
    def test_to_dto(self):
        with open(Path('test/fixtures/example.json').absolute()) as file:
            source_file = json.load(file)

        field = PrivateCamelCaseFieldStrategy()
        field.build_field = MagicMock(name='build_field')
        field_expected_calls = [
            call('id', '29738e61-7f6a-11e8-ac45-09db60ede9d6'),
            call('companyName', ''),
            call('guest', False),
            call('personalIdentity', '67062589524'),
            call('phoneNumber', 123456789),
            call('countryCode', 'PL'),
            call('address', {"countryCode": "PL"}),
            call('buyer', {
                "companyName": "",
                "guest": False,
                "personalIdentity": "67062589524",
                "phoneNumber": 123456789,
                "address": {
                    "countryCode": "PL"
                }
            }),
            call('id', '62ae358b-8f65-4fc4-9c77-bedf604a2e2b'),
            call('name', 'Name of purchased offer'),
            call('id', 'AH-129834'),
            call('external', {
                "id": "AH-129834"
            }),
            call('offer', {
                "name": "Name of purchased offer",
                "external": {
                    "id": "AH-129834"
                }
            }),
            call('quantity', 1),
            call('lineItems', [
                {
                    "id": "62ae358b-8f65-4fc4-9c77-bedf604a2e2b",
                    "offer": {
                        "name": "Name of purchased offer",
                        "external": {
                            "id": "AH-129834"
                        }
                    },
                    "quantity": 1
                }
            ]),
            call('updatedAt', '2011-12-03T10:15:30.133Z')
        ]
        field.build_field.side_effect = [
            dto.FieldDto('private', 'id', 'string'),
            dto.FieldDto('private', 'companyName', 'string'),
            dto.FieldDto('private', 'guest', 'boolean'),
            dto.FieldDto('private', 'personalIdentity', 'string'),
            dto.FieldDto('private', 'phoneNumber', 'int'),
            dto.FieldDto('private', 'countryCode', 'string'),
            dto.FieldDto('private', 'address', 'Test/Name/Space/Address'),
            dto.FieldDto('private', 'buyer', 'Test/Name/Space/Buyer'),
            dto.FieldDto('private', 'id', 'string'),
            dto.FieldDto('private', 'name', 'string'),
            dto.FieldDto('private', 'id', 'string'),
            dto.FieldDto('private', 'external', 'Test/Name/Space/External'),
            dto.FieldDto('private', 'offer', 'Test/Name/Space/Offer'),
            dto.FieldDto('private', 'quantity', 'int'),
            dto.FieldDto('private', 'lineItems', 'LineItem[]'),
            dto.FieldDto('private', 'updatedAt', 'string'),
        ]

        jms = JmsAnnotationSerializerStrategy()
        jms.add_serialization = MagicMock(name='add_serialization')
        jms_expected_calls = [
            call(dto.FieldDto('private', 'id', 'string'), dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'companyName', 'string'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'guest', 'boolean'), dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'personalIdentity', 'string'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'phoneNumber', 'int'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'countryCode', 'string'),
                 dto.ClassDto(name='Address', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'address', 'Test/Name/Space/Address'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'buyer', 'Test/Name/Space/Buyer'),
                 dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'id', 'string'), dto.ClassDto(name='LineItems', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'name', 'string'), dto.ClassDto(name='Offer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'id', 'string'), dto.ClassDto(name='External', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'external', 'Test/Name/Space/External'),
                 dto.ClassDto(name='Offer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'offer', 'Test/Name/Space/Offer'),
                 dto.ClassDto(name='LineItems', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'quantity', 'int'),
                 dto.ClassDto(name='LineItems', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'lineItems', 'LineItem[]'),
                 dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'updatedAt', 'string'),
                 dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
        ]
        jms.add_serialization.side_effect = [
            dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'),
            dto.FieldDto('private', 'companyName', 'string', None, '@JMS/Serialization("string")'),
            dto.FieldDto('private', 'guest', 'boolean', None, '@JMS/Serialization("bool")'),
            dto.FieldDto('private', 'personalIdentity', 'string', None, '@JMS/Serialization("string")'),
            dto.FieldDto('private', 'phoneNumber', 'int', None, '@JMS/Serialization("int")'),
            dto.FieldDto('private', 'countryCode', 'string', None, '@JMS/Serialization("string")'),
            dto.FieldDto('private', 'address', 'Address', None, '@JMS/Serialization("Test/Name/Space/Address")'),
            dto.FieldDto('private', 'buyer', 'Buyer', None, '@JMS/Serialization("Test/Name/Space/Buyer")'),
            dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'),
            dto.FieldDto('private', 'name', 'string', None, '@JMS/Serialization("string")'),
            dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'),
            dto.FieldDto('private', 'external', 'External', None, '@JMS/Serialization("Test/Name/Space/External")'),
            dto.FieldDto('private', 'offer', 'Offer', None, '@JMS/Serialization("Test/Name/Space/Offer")'),
            dto.FieldDto('private', 'quantity', 'int', None, '@JMS/Serialization("integer")'),
            dto.FieldDto('private', 'lineItems', 'LineItem[]', None,
                         '@JMS/Serialization("array<Test/Name/Space/Buyer/LineItem>")'),
            dto.FieldDto('private', 'updatedAt', 'string', None, '@JMS/Serialization("string")'),
        ]

        getter = PublicCamelCaseStrictTypesGetterStrategy()
        getter.build_getter = MagicMock(name='build_getter')
        getter_expected_calls = [
            call(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")')),
            call(dto.FieldDto('private', 'companyName', 'string', None, '@JMS/Serialization("string")')),
            call(dto.FieldDto('private', 'guest', 'boolean', None, '@JMS/Serialization("bool")')),
            call(dto.FieldDto('private', 'personalIdentity', 'string', None, '@JMS/Serialization("string")')),
            call(dto.FieldDto('private', 'phoneNumber', 'int', None, '@JMS/Serialization("int")')),
            call(dto.FieldDto('private', 'countryCode', 'string', None, '@JMS/Serialization("string")')),
            call(dto.FieldDto('private', 'address', 'Address', None, '@JMS/Serialization("Test/Name/Space/Address")')),
            call(dto.FieldDto('private', 'buyer', 'Buyer', None, '@JMS/Serialization("Test/Name/Space/Buyer")')),
            call(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")')),
            call(dto.FieldDto('private', 'name', 'string', None, '@JMS/Serialization("string")')),
            call(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")')),
            call(dto.FieldDto('private', 'external', 'External', None,
                              '@JMS/Serialization("Test/Name/Space/External")')),
            call(dto.FieldDto('private', 'offer', 'Offer', None, '@JMS/Serialization("Test/Name/Space/Offer")')),
            call(dto.FieldDto('private', 'quantity', 'int', None, '@JMS/Serialization("integer")')),
            call(dto.FieldDto('private', 'lineItems', 'LineItem[]', None,
                              '@JMS/Serialization("array<Test/Name/Space/Buyer/LineItem>")')),
            call(dto.FieldDto('private', 'updatedAt', 'string', None, '@JMS/Serialization("string")')),
        ]
        getter.build_getter.side_effect = [
            dto.GetterDto(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'getId'),
            dto.GetterDto(dto.FieldDto('private', 'companyName', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'getCompanyName'),
            dto.GetterDto(dto.FieldDto('private', 'guest', 'boolean', None, '@JMS/Serialization("bool")'), 'public',
                          'getGuest'),
            dto.GetterDto(dto.FieldDto('private', 'personalIdentity', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'getPersonalIdentity'),
            dto.GetterDto(dto.FieldDto('private', 'phoneNumber', 'int', None, '@JMS/Serialization("int")'), 'public',
                          'getPhoneNumber'),
            dto.GetterDto(dto.FieldDto('private', 'countryCode', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'getCountryCode'),
            dto.GetterDto(
                dto.FieldDto('private', 'address', 'Address', None, '@JMS/Serialization("Test/Name/Space/Address")'),
                'public', 'getAddress'),
            dto.GetterDto(
                dto.FieldDto('private', 'buyer', 'Buyer', None, '@JMS/Serialization("Test/Name/Space/Buyer")'),
                'public', 'getBuyer'),
            dto.GetterDto(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'getId'),
            dto.GetterDto(dto.FieldDto('private', 'name', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'getName'),
            dto.GetterDto(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'getId'),
            dto.GetterDto(
                dto.FieldDto('private', 'external', 'External', None, '@JMS/Serialization("Test/Name/Space/External")'),
                'public', 'getExternal'),
            dto.GetterDto(
                dto.FieldDto('private', 'offer', 'Offer', None, '@JMS/Serialization("Test/Name/Space/Offer")'),
                'public', 'getOffer'),
            dto.GetterDto(dto.FieldDto('private', 'quantity', 'int', None, '@JMS/Serialization("integer")'), 'public',
                          'getQuantity'),
            dto.GetterDto(dto.FieldDto('private', 'lineItems', 'LineItem[]', None,
                                       '@JMS/Serialization("array<Test/Name/Space/Buyer/LineItem>")'), 'public',
                          'getLineItems'),
            dto.GetterDto(dto.FieldDto('private', 'updatedAt', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'getUpdatedAt'),
        ]

        setter = PublicCamelCaseFluentStrictTypesSetterStrategy()
        setter.build_setter = MagicMock(name='build_setter')
        setter_expected_calls = [
            call(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'companyName', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'guest', 'boolean', None, '@JMS/Serialization("bool")'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'personalIdentity', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'phoneNumber', 'int', None, '@JMS/Serialization("int")'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'countryCode', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='Address', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'address', 'Address', None, '@JMS/Serialization("Test/Name/Space/Address")'),
                 dto.ClassDto(name='Buyer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'buyer', 'Buyer', None, '@JMS/Serialization("Test/Name/Space/Buyer")'),
                 dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='LineItems', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'name', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='Offer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='External', namespace='Test/Name/Space')),
            call(
                dto.FieldDto('private', 'external', 'External', None, '@JMS/Serialization("Test/Name/Space/External")'),
                dto.ClassDto(name='Offer', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'offer', 'Offer', None, '@JMS/Serialization("Test/Name/Space/Offer")'),
                 dto.ClassDto(name='LineItems', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'quantity', 'int', None, '@JMS/Serialization("integer")'),
                 dto.ClassDto(name='LineItems', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'lineItems', 'LineItem[]', None,
                              '@JMS/Serialization("array<Test/Name/Space/Buyer/LineItem>")'),
                 dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
            call(dto.FieldDto('private', 'updatedAt', 'string', None, '@JMS/Serialization("string")'),
                 dto.ClassDto(name='RootClass', namespace='Test/Name/Space')),
        ]
        setter.build_setter.side_effect = [
            dto.SetterDto(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'setId', True, 'RootClass'),
            dto.SetterDto(dto.FieldDto('private', 'companyName', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'setCompanyName', True, 'Buyer'),
            dto.SetterDto(dto.FieldDto('private', 'guest', 'boolean', None, '@JMS/Serialization("bool")'), 'public',
                          'setGuest', True, 'Buyer'),
            dto.SetterDto(dto.FieldDto('private', 'personalIdentity', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'setPersonalIdentity', True, 'Buyer'),
            dto.SetterDto(dto.FieldDto('private', 'phoneNumber', 'int', None, '@JMS/Serialization("int")'), 'public',
                          'setPhoneNumber', True, 'Buyer'),
            dto.SetterDto(dto.FieldDto('private', 'countryCode', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'setCountryCode', True, 'Address'),
            dto.SetterDto(
                dto.FieldDto('private', 'address', 'Address', None, '@JMS/Serialization("Test/Name/Space/Address")'),
                'public', 'setAddress', True, 'Buyer'),
            dto.SetterDto(
                dto.FieldDto('private', 'buyer', 'Buyer', None, '@JMS/Serialization("Test/Name/Space/Buyer")'),
                'public', 'setBuyer', True, 'RootClass'),
            dto.SetterDto(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'setId', True, 'LineItems'),
            dto.SetterDto(dto.FieldDto('private', 'name', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'setName', True, 'Offer'),
            dto.SetterDto(dto.FieldDto('private', 'id', 'string', None, '@JMS/Serialization("string")'), 'public',
                          'setId', True, 'External'),
            dto.SetterDto(
                dto.FieldDto('private', 'external', 'External', None, '@JMS/Serialization("Test/Name/Space/External")'),
                'public', 'setExternal', True, 'Offer'),
            dto.SetterDto(
                dto.FieldDto('private', 'offer', 'Offer', None, '@JMS/Serialization("Test/Name/Space/Offer")'),
                'public', 'setOffer', True, 'LineItems'),
            dto.SetterDto(dto.FieldDto('private', 'quantity', 'integer', None, '@JMS/Serialization("integer")'),
                          'public', 'setQuantity', True, 'LineItems'),
            dto.SetterDto(dto.FieldDto('private', 'lineItems', 'LineItem[]', None,
                                       '@JMS/Serialization("array<Test/Name/Space/Buyer/LineItem>")'), 'public',
                          'setLineItems', True, 'RootClass'),
            dto.SetterDto(dto.FieldDto('private', 'updatedAt', 'string', None, '@JMS/Serialization("string")'),
                          'public', 'setUpdatedAt', True, 'RootClass'),
        ]

        transformer = JsonTransformer(field, getter, setter, jms)
        class_dtos = transformer.to_dto(source_file, False, 'Test/Name/Space', 'RootClass')

        self.assertEqual(field.build_field.mock_calls, field_expected_calls)
        self.assertEqual(len(jms.add_serialization.mock_calls), len(jms_expected_calls))
        self.assertEqual(len(getter.build_getter.mock_calls), len(getter_expected_calls))
        self.assertEqual(len(setter.build_setter.mock_calls), len(setter_expected_calls))
        self.assertEqual(len(class_dtos), 6)

        address_class = class_dtos['Address']
        self.assertEqual(address_class.name, 'Address')
        self.assertEqual(address_class.namespace, 'Test/Name/Space')
        self.assertEqual(1, len(address_class.fields))
        self.assertEqual(1, len(address_class.getters))
        self.assertEqual(1, len(address_class.setters))
        self.assertEqual(address_class.dependencies,
                         [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")])

        address_class = class_dtos['Buyer']
        self.assertEqual(address_class.name, 'Buyer')
        self.assertEqual(address_class.namespace, 'Test/Name/Space')
        self.assertEqual(5, len(address_class.fields))
        self.assertEqual(5, len(address_class.getters))
        self.assertEqual(5, len(address_class.setters))
        self.assertEqual(address_class.dependencies,
                         [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")])

        address_class = class_dtos['LineItems']
        self.assertEqual(address_class.name, 'LineItems')
        self.assertEqual(address_class.namespace, 'Test/Name/Space')
        self.assertEqual(3, len(address_class.fields))
        self.assertEqual(3, len(address_class.getters))
        self.assertEqual(3, len(address_class.setters))
        self.assertEqual(address_class.dependencies,
                         [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")])

        address_class = class_dtos['External']
        self.assertEqual(address_class.name, 'External')
        self.assertEqual(address_class.namespace, 'Test/Name/Space')
        self.assertEqual(1, len(address_class.fields))
        self.assertEqual(1, len(address_class.getters))
        self.assertEqual(1, len(address_class.setters))
        self.assertEqual(address_class.dependencies,
                         [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")])

        address_class = class_dtos['Offer']
        self.assertEqual(address_class.name, 'Offer')
        self.assertEqual(address_class.namespace, 'Test/Name/Space')
        self.assertEqual(2, len(address_class.fields))
        self.assertEqual(2, len(address_class.getters))
        self.assertEqual(2, len(address_class.setters))
        self.assertEqual(address_class.dependencies,
                         [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")])

        address_class = class_dtos['RootClass']
        self.assertEqual(address_class.name, 'RootClass')
        self.assertEqual(address_class.namespace, 'Test/Name/Space')
        self.assertEqual(4, len(address_class.fields))
        self.assertEqual(4, len(address_class.getters))
        self.assertEqual(4, len(address_class.setters))
        self.assertEqual(address_class.dependencies,
                         [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")])


if __name__ == '__main__':
    unittest.main()
