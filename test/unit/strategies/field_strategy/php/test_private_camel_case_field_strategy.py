import unittest
import strategies

field_parameters = [
    ('test', 'test', 'string'),
    ('test', 1, 'integer'),
    ('test', 1.4, 'float'),
    ('test', True, 'boolean'),
    ('test', None, 'null'),
    ('test', ['a'], 'array'),
    ('test', [], 'array'),
    ('foo_class', {'test': 1}, 'FooClass'),
    ('foo_class', [{"test": 1}], 'FooClass[]'),
]


class PrivateCamelCaseFieldStrategyTest(unittest.TestCase):
    def test_supported_prog_lang(self):
        self.assertEqual(1, strategies.field_strategy.PrivateCamelCaseFieldStrategy.supported_prog_lang().value)

    def test_supported_min_version(self):
        self.assertEqual('7.4', strategies.field_strategy.PrivateCamelCaseFieldStrategy.supported_min_ver())

    def test_build_field(self):
        strategy = strategies.field_strategy.PrivateCamelCaseFieldStrategy()
        for key, value, expected_type in field_parameters:
            with self.subTest():
                field = strategy.build_field(key, value)
                self.assertEqual('private', field.access)
                self.assertEqual(key, field.name)
                self.assertEqual(expected_type, field.field_type)
                self.assertEqual(None, field.init_value)
                self.assertEqual(None, field.serialization)


if __name__ == '__main__':
    unittest.main()
