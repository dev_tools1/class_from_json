import unittest
import strategies


class SameAsClassNameStrategyTest(unittest.TestCase):
    def test_build_file_name(self):
        strategy = strategies.file_name_strategy.SameAsClassNameStrategy()
        self.assertEqual('test.php', strategy.build_file_name('test', '.php'))


if __name__ == '__main__':
    unittest.main()
