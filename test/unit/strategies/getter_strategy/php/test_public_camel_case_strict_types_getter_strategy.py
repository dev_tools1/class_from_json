import unittest
import strategies
from dto import FieldDto

getter_info = [
    (FieldDto('test', 'test', 'test', 'test', 'test'), 'getTest'),
    (FieldDto('test', '_test', 'test', 'test', 'test'), 'get_test'),
    (FieldDto('test', '1test', 'test', 'test', 'test'), 'get1test'),
    (FieldDto('test', 'TestAbcDef', 'test', 'test', 'test'), 'getTestAbcDef'),
    (FieldDto('test', 'testAbcDefEg', 'test', 'test', 'test'), 'getTestAbcDefEg'),
]


class PublicCamelCaseStrictTypesGetterStrategyTest(unittest.TestCase):
    def test_supported_prog_lang(self):
        self.assertEqual(1,
                         strategies.getter_strategy.PublicCamelCaseStrictTypesGetterStrategy.supported_prog_lang().value)

    def test_supported_min_version(self):
        self.assertEqual('7.4', strategies.getter_strategy.PublicCamelCaseStrictTypesGetterStrategy.supported_min_ver())

    def test_build_getter(self):
        getter_strategy = strategies.getter_strategy.PublicCamelCaseStrictTypesGetterStrategy()
        for field_dto, expected_name in getter_info:
            getter = getter_strategy.build_getter(field_dto)
            self.assertEqual(field_dto, getter.field)
            self.assertEqual('public', getter.access)
            self.assertEqual(expected_name, getter.func_name)


if __name__ == '__main__':
    unittest.main()
