import unittest
from strategies.null_strategy.php.standard_null_strategy import StandardNullStrategy
from dto import FieldDto

nullable_info = [
    (FieldDto('test', 'test', 'test', 'test', 'test'), '?test', 'test'),
    (FieldDto('test', '_test', 'test', '', 'test'), '?test', 'null'),
    (FieldDto('test', '_test', 'test', None, 'test'), '?test', 'null'),
    (FieldDto('test', '_test', 'test', '  ', 'test'), '?test', '  '),
]


class StandardNullStrategyTest(unittest.TestCase):
    def test_supported_prog_lang(self):
        self.assertEqual(1, StandardNullStrategy.supported_prog_lang().value)

    def test_supported_min_version(self):
        self.assertEqual('7.4', StandardNullStrategy.supported_min_ver())

    def test_make_nullable(self):
        strategy = StandardNullStrategy()
        for field_dto, expected_type, expected_init_value in nullable_info:
            with self.subTest():
                field = strategy.make_nullable(field_dto)
                self.assertEqual(expected_type, field.field_type)
                self.assertEqual(expected_init_value, field.init_value)


if __name__ == '__main__':
    unittest.main()
