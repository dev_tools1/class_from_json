import unittest
from strategies.serializer_strategy.php.jms_annotation_serializer_strategy import JmsAnnotationSerializerStrategy
import dto

serialization_info = [
    (dto.FieldDto('test', 'test', 'string', 'test'), dto.ClassDto(name='test', namespace='Test/test/test/A'),
     '@Type("string")'),
    (dto.FieldDto('test', 'test', 'int', 'test'), dto.ClassDto(name='test', namespace='Test/test/test/A'),
     '@Type("int")'),
    (dto.FieldDto('test', 'test', 'array', 'test'), dto.ClassDto(name='test', namespace='Test/test/test/A'),
     '@Type("array")'),
    (dto.FieldDto('test', 'test', 'bool', 'test'), dto.ClassDto(name='test', namespace='Test/test/test/A'),
     '@Type("bool")'),
    (dto.FieldDto('test', 'test', 'float', 'test'), dto.ClassDto(name='test', namespace='Test/test/test/A'),
     '@Type("float")'),
    (dto.FieldDto('test', 'test', 'FooClass', 'test'), dto.ClassDto(name='test', namespace='Test/test/test/A'),
     '@Type("Test/test/test/A/FooClass")'),
    (dto.FieldDto('test', 'test', 'FooClass[]', 'test'), dto.ClassDto(name='test', namespace='Test/test/test/A'),
     '@Type("array<Test/test/test/A/FooClass>")'),
]


class JmsAnnotationSerializerStrategyTest(unittest.TestCase):
    def test_supported_prog_lang(self):
        self.assertEqual(1, JmsAnnotationSerializerStrategy.supported_prog_lang().value)

    def test_supported_min_version(self):
        self.assertEqual('7.4', JmsAnnotationSerializerStrategy.supported_min_ver())

    def test_required_dependencies(self):
        self.assertEqual(
            [dto.ReqDependenciesDto('^3.12', 'jms/serializer', r"JMS\Serializer\Annotation\Type")],
            JmsAnnotationSerializerStrategy.required_dependencies()
        )

    def test_add_serialization(self):
        strategy = JmsAnnotationSerializerStrategy()
        for field_dto, class_dto, expected_serialization in serialization_info:
            with self.subTest():
                field = strategy.add_serialization(field_dto, class_dto)
                self.assertEqual(expected_serialization, field.serialization)


if __name__ == '__main__':
    unittest.main()
