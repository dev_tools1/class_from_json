import unittest
import dto
from strategies.setter_strategy.php.public_camel_case_fluent_strict_types_setter_strategy import \
    PublicCamelCaseFluentStrictTypesSetterStrategy

setter_info = [
    (dto.FieldDto('test', 'test', 'test', 'test'), dto.ClassDto(name='test', namespace='test'), 'setTest', 'test'),
    (dto.FieldDto('test', 'testAbcDcf', 'test', 'test'), dto.ClassDto(name='test', namespace='test'), 'setTestAbcDcf', 'test'),
    (dto.FieldDto('test', '_aaa', 'test', 'test'), dto.ClassDto(name='test', namespace='test'), 'set_aaa', 'test'),
    (dto.FieldDto('test', '123AAAA', 'test', 'test'), dto.ClassDto(name='test', namespace='test'), 'set123AAAA', 'test'),
    (dto.FieldDto('test', 'TestAbc', 'test', 'test'), dto.ClassDto(name='test', namespace='test'), 'setTestAbc', 'test'),
    (dto.FieldDto('test', ' A', 'test', 'test'), dto.ClassDto(name='test', namespace='test'), 'set A', 'test'),
]


class PublicCamelCaseFluentStrictTypesSetterStrategyTest(unittest.TestCase):
    def test_supported_prog_lang(self):
        self.assertEqual(1, PublicCamelCaseFluentStrictTypesSetterStrategy.supported_prog_lang().value)

    def test_supported_min_version(self):
        self.assertEqual('7.4', PublicCamelCaseFluentStrictTypesSetterStrategy.supported_min_ver())

    def test_build_setter(self):
        strategy = PublicCamelCaseFluentStrictTypesSetterStrategy()
        for field_dto, class_dto, expected_set_name, expected_return_type in setter_info:
            with self.subTest():
                setter = strategy.build_setter(field_dto, class_dto)
                self.assertEqual(field_dto, setter.field)
                self.assertEqual('public', setter.access)
                self.assertEqual(expected_set_name, setter.func_name)
                self.assertEqual(True, setter.is_fluent)
                self.assertEqual(expected_return_type, setter.return_type)


if __name__ == '__main__':
    unittest.main()
