import unittest
from factory import StrategyFactory
import strategies

strategy_list = [
    ('field_strategy', 'PrivateCamelCaseFieldStrategy', strategies.field_strategy.PrivateCamelCaseFieldStrategy),
    ('getter_strategy', 'PublicCamelCaseStrictTypesGetterStrategy',
     strategies.getter_strategy.PublicCamelCaseStrictTypesGetterStrategy),
    ('setter_strategy', 'PublicCamelCaseFluentStrictTypesSetterStrategy',
     strategies.setter_strategy.PublicCamelCaseFluentStrictTypesSetterStrategy),
    ('serializer_strategy', 'JmsAnnotationSerializerStrategy',
     strategies.serializer_strategy.JmsAnnotationSerializerStrategy),
    ('file_name_strategy', 'SameAsClassNameStrategy', strategies.file_name_strategy.SameAsClassNameStrategy),
    ('null_strategy', 'StandardNullStrategy', strategies.null_strategy.StandardNullStrategy),
]


class StrategyFactoryTest(unittest.TestCase):
    def test_build(self, ):
        for strategy_type, class_name, expected_class in strategy_list:
            with self.subTest():
                strategy = StrategyFactory.build(strategy_type, class_name)
                self.assertIsInstance(strategy, expected_class)


if __name__ == '__main__':
    unittest.main()
